﻿using System.Numerics;

namespace Lottozahlen
{
    class StatisticsHelper
    {
        public static BigInteger Faculty(int number)
        {
            if (number <= 1)
            {
                return 1;
            }
            return number * Faculty(number - 1);
        }

        public static double CalculateChance(int drawCount, int totalCount, int correctNumbers)
        {
            int remainingNumbers = totalCount - drawCount;
            int wrongNumbers = drawCount - correctNumbers;

            BigInteger a = Faculty(drawCount) / (Faculty(correctNumbers) * Faculty(drawCount - correctNumbers));
            BigInteger b = Faculty(remainingNumbers) / (Faculty(wrongNumbers) * Faculty(remainingNumbers - wrongNumbers));
            BigInteger c = Faculty(totalCount) / (Faculty(drawCount) * Faculty(totalCount - drawCount));

            return ((double)a * (double)b) / (double)c;
        }
    }
}
