﻿using System;
using System.ComponentModel;

namespace Lottozahlen
{
    class Configuration : INotifyPropertyChanged
    {
        private static int DefaultCount = 6;
        private static int DefaultMinValue = 1;
        private static int DefaultMaxValue = 45;

        public event PropertyChangedEventHandler PropertyChanged;

        private int _absoluteMinimum = 1;
        private int _absoluteMaximum = 100;

        private int _count;
        private int _minValue;
        private int _maxValue;

        public int Count
        {
            get => _count;
            set
            {
                _count = value;

                if (_maxValue - _minValue < _count)
                {
                    int newMaxValue = Math.Min(AbsoluteMaximum, _minValue + _count);
                    int difference = (_minValue + _count) - newMaxValue;

                    if (difference > 0)
                    {
                        _minValue -= difference;
                        MinValue = _minValue;
                    }

                    MaxValue = newMaxValue;
                }

                OnPropertyChanged("Count");
            }
        }
        public int MinValue
        {
            get => _minValue;
            set
            {
                _minValue = value;

                if (_maxValue < (_minValue + _count))
                {
                    _maxValue = _minValue + _count;
                    if (_maxValue > AbsoluteMaximum)
                    {
                        _maxValue = AbsoluteMaximum;
                        _minValue = _maxValue - _count;
                    }
                    MaxValue = _maxValue;
                }

                OnPropertyChanged("MinValue");
            }
        }
        public int MaxValue
        {
            get => _maxValue;
            set
            {
                _maxValue = value;

                if (_minValue > (_maxValue - _count))
                {
                    _minValue = _maxValue - _count;
                    if (_minValue < AbsoluteMinimum)
                    {
                        _minValue = AbsoluteMinimum;
                        _maxValue = _minValue + _count;
                    }
                    MinValue = _minValue;
                }

                OnPropertyChanged("MaxValue");
            }
        }

        public int AbsoluteMinimum
        {
            get => _absoluteMinimum;
            private set
            {
                _absoluteMinimum = value;
                OnPropertyChanged("AbsoluteMinimum");
            }
        }

        public int AbsoluteMaximum
        {
            get => _absoluteMaximum;
            private set
            {
                _absoluteMaximum = value;
                OnPropertyChanged("AbsoluteMaximum");
            }
        }

        public Configuration()
        {
            _count = DefaultCount;
            _minValue = DefaultMinValue;
            _maxValue = DefaultMaxValue;
        }

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void SetDefaultValues()
        {
            Count = DefaultCount;
            MinValue = DefaultMinValue;
            MaxValue = DefaultMaxValue;
        }

    }
}
