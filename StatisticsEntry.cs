﻿namespace Lottozahlen
{
    public class StatisticsEntry
    {
        private int _totalNumbers;
        private int _drawnNumbers;
        private int _correctNumbers;
        public double WinningChance { get; private set; }

        public string WinningPercentage
        {
            get => (WinningChance * 100).ToString("###0." + new string('0', 12)) + " %";
        }

        public string Label
        {
            get => $"{_correctNumbers} richtige aus {_totalNumbers}";
        }

        public StatisticsEntry(int totalNumbers, int drawnNumbers, int correctNumbers)
        {
            _totalNumbers = totalNumbers;
            _drawnNumbers = drawnNumbers;
            _correctNumbers = correctNumbers;

            WinningChance = StatisticsHelper.CalculateChance(_drawnNumbers, _totalNumbers, _correctNumbers);
        }
    }
}