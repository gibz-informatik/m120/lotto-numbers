﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;

namespace Lottozahlen
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Configuration _configuration = new Configuration();

        private int[] _numbers;

        public ObservableCollection<StatisticsEntry> Statistics { get; set; }

        public int Count { get => _configuration.Count; set => _configuration.Count = value; }
        public int MinValue { get => _configuration.MinValue; set => _configuration.MinValue = value; }
        public int MaxValue { get => _configuration.MaxValue; set => _configuration.MaxValue = value; }

        public MainWindow()
        {
            InitializeComponent();
            Statistics = new ObservableCollection<StatisticsEntry>();
            DataContext = _configuration;
            StatisticsListBox.DataContext = this;
        }

        private void SetDefaultValues(object sender, RoutedEventArgs e)
        {
            _configuration.SetDefaultValues();
        }

        private void GenerateNumbers(object sender, RoutedEventArgs e)
        {
            _numbers = new int[Count];
            Random numberGenerator = new Random();

            for (int i = 0; i < _numbers.Length; i++)
            {
                int randomNumber;
                do
                {
                    randomNumber = numberGenerator.Next(MinValue, MaxValue + 1);
                } while (_numbers.Contains(randomNumber));
                _numbers[i] = randomNumber;
            }

            Array.Sort(_numbers);

            txtNumbers.Text = string.Join(" | ", _numbers);

            Statistics.Clear();
            for (int correctNumbers = 1; correctNumbers <= Count; correctNumbers++)
            {
                Statistics.Add(new StatisticsEntry(MaxValue - MinValue + 1, Count, correctNumbers));
            }
        }
    }
}